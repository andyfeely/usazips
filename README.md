# WaratekProject

*** Available at: usazips.andyfeely.com ***

Tested on Chrome on my desktop and Mobile screen (Samsung Galaxy s7)

This project is to demonstrate my understanding and skill level of frontend development using a javascript framework.
I have used Angular 4. Based on the brief which was to utilise the json file that was given to me I decided to design
a single page application which pulls the zipcodes from the file and displays them to the user using Google Maps API.

The development process took roughly 4.5 hours. I challenged myself by not spending more time as I wanted to see personally
what I could achieve in that time. If I had more time there is a lot more I could do with the data. For example 
plotting distance times driving between zip codes or plotting the fastest route to an area if you were a parcel delivery service.

The app itself allows the user to zoom into an area and the zip codes are pulled for that area. They can also search for zip codes and add them to 
a selection array. 

In order to install you must have NPM installed and just key "npm install" in the root directory, and then npm start to initialize.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
