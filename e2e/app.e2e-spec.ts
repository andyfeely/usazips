import { WaratekProjectPage } from './app.po';

describe('waratek-project App', () => {
  let page: WaratekProjectPage;

  beforeEach(() => {
    page = new WaratekProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
