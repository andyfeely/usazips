import {Component, HostListener, OnInit} from '@angular/core';
import { Zip } from './zip/zip';
import { ZipService } from './zip/zip.service';
import { State } from './zip/state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  localZips: Zip[] = [];
  showLocal: boolean;
  searchZips: Zip[] = [];
  filteredZips: Zip[] = [];
  states: State[];
  hideMenu: boolean;
  lat: number = 39.02771884;
  lng: number = -100.59082031;
  zoom = 4;
  zoomLevel = 0;
  mapHeight: number = 500;

  constructor( private zipService: ZipService) {  }

  ngOnInit() {
    this.getWindowHeight();
    this.zipService.localZips.subscribe(res => this.localZips = res);
    this.zipService.searchZips.subscribe(res => this.searchZips = res);
    this.zipService.filteredZips.subscribe(res =>this.filteredZips = res);
    this.zipService.states.subscribe(res => this.states = res);
    this.zipService.showLocal.subscribe(res => this.showLocal = res);
    this.zipService.hideMenu.subscribe(res => this.hideMenu = res);
  }

  clickedMarker(label: string, i: number){
    console.log(label);
    console.log(i);
  }

  zoomChange(event: number){
    this.zoomLevel = +event;
  }

  boundsChanged(event: Event){
    this.zipService.longtitude = [event['b']['b'], event['b']['f']];
    this.zipService.latitude = [event['f']['b'], event['f']['f']];
    if (this.zoomLevel > 9 ) {
      this.zipService.updateZips();
    } else {
      this.zipService.localZips.next([])
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.getWindowHeight();
  }

  getWindowHeight(){
    this.mapHeight = window.innerHeight - 85;
  }
}

interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}
