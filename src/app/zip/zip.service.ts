import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { BehaviorSubject, Observable } from 'rxjs';
import { Zip } from './zip';
import { State } from "./state";


@Injectable()
export class ZipService {

  public _zips: BehaviorSubject<Zip[]> = new BehaviorSubject(null);
  public localZips: BehaviorSubject<Zip[]> = new BehaviorSubject(null);
  public showLocal: BehaviorSubject<boolean> = new BehaviorSubject(true);
  public searchZips: BehaviorSubject<Zip[]> = new BehaviorSubject(null);
  public findZips: BehaviorSubject<Zip[]> = new BehaviorSubject(null);
  public filteredZips: BehaviorSubject<Zip[]> = new BehaviorSubject(null);
  public states: BehaviorSubject<State[]> = new BehaviorSubject(null);
  public longtitude: number[] = [179.42871093999997, 6.987304689999974];
  public latitude: number[] = [60.11596968727254, 16.56600242811442];
  public errorMessage: BehaviorSubject<string> = new BehaviorSubject(null);
  public hideMenu: BehaviorSubject<boolean> = new BehaviorSubject(true);

  static convertToJson(s: string) {
    const lines = s.split('\n');
    const zips: Zip[] = [];
    for (const line of lines.splice(0, 30000)){
      if (line.length > 0) {
        zips.push(JSON.parse(line));
      }
    }
    return zips;
  }

  constructor( private http: Http ) {
    this.getZips();
  }

  getZips() {
    this.http.get('assets/zips.json')
      .map( res => ZipService.convertToJson(res.text()))
      .subscribe(res => {
        this._zips.next(res);
      });
  }

  getStates() {
    let states: State[] = [];
    for (const zip of this._zips.getValue()){
      let state = states.filter( state => state.name === zip.state);
      if (state.length === 0) {
        states.push( new State( zip.state, [zip]));
      } else {
        state[0].zips.push(zip);
      }
    }
    for (let state of states){
      state.getPopulation();
      state.getLocation();
    }
    this.states.next(states);
  }

  changeShowStates(){
    if (this.states.getValue() !== null){
      this.states.next(null);
    } else {
      this.getStates();
    }
  }

  updateZips() {
    let zips: Zip[] = [];
    for (const zip of this._zips.getValue()){
      if (zip.loc[0] > this.longtitude[0] && zip.loc[0] < this.longtitude[1]){
        if ( zip.loc[1] < this.latitude[0] && zip.loc[1] > this.latitude[1])
        {
          zips.push(zip);
        }
      }
    }
    this.localZips.next(zips);
  }

  filterZips(popG: number, popL: number) {
    let zips: Zip[] = [];
    for (const zip of this._zips.getValue()){
      if (zip.pop > popG && zip.pop < popL){
        zips.push(zip);
      }
    }
    if (zips.length > 1000){
      this.errorMessage.next("Too many zips to display");
      this.searchZips.next([]);
    } else {
      this.searchZips.next(zips);
    }
  }

  findZip(zipString: string){
    if (zipString === ""){
      this.findZips.next([]);
    } else{
      let zips = this._zips.getValue().filter(item => item._id.indexOf(zipString) !== -1);
      this.findZips.next(zips.splice(0, 5));
    }
  }
}
