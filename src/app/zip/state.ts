import { Zip } from './zip';

export class State {
  name: string;
  zips: Zip[];
  location: number[];
  population: number;

  constructor(name: string, zips: Zip[]) {
    this.name = name;
    this.zips = zips;
  }

  getPopulation() {
    let p = 0;
    for (const zip of this.zips){
      p += zip.pop;
    }
    this.population = p;
  }

  getLocation(){
    let l = [0, 0];
    for (const zip of this.zips){
      l[0] += zip.loc[0];
      l[1] += zip.loc[1];
    }
    this.location = [l[0] / this.zips.length, l[1] / this.zips.length];
  }
}
