
export class Zip {
  city: string;
  loc: number[];
  pop: number;
  state: string;
  _id: string;

  constructor(city: string, loc: number[], pop: number, state: string){
    this.city = city;
    this.loc = loc;
    this.pop = pop;
    this.state = state;
  }
}
