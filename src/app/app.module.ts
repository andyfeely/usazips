import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { ZipService } from './zip/zip.service';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { NavbarComponent } from './navbar/navbar.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBV0EujgkTpcMWPsw2hLsAHNQIXmRvMs9E'
    })
  ],
  providers: [ ZipService ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
