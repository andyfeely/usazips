import { Component, OnInit } from '@angular/core';
import { ZipService } from "../zip/zip.service";
import { Zip } from "../zip/zip";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  allZips: Zip[];
  popG: number;
  popL: number;
  errorMessage: string;
  hideMenu: boolean;
  selectedZip: string;
  selectedZips: Zip[] = [];
  findZips: Zip[] = [];

  constructor( private zipService: ZipService) {
  }

  ngOnInit() {
    this.zipService.errorMessage.subscribe(res => this.errorMessage = res);
    this.zipService._zips.subscribe(res => this.allZips = res);
    this.zipService.findZips.subscribe(res => this.findZips = res);
    this.zipService.hideMenu.subscribe(res => this.hideMenu = res);
  }

  changeShowStates(){
    this.zipService.changeShowStates();
  }

  changeShowLocal(){
    this.zipService.showLocal.next(!this.zipService.showLocal.getValue());
  }

  getZips(){
    this.zipService.filterZips(this.popG ? this.popG : 0, this.popL ? this.popL : 9999999);
  }

  searchZips(){
    this.zipService.findZip(this.selectedZip);
  }

  addZip(zip: Zip){
    if(this.selectedZips.filter(item => item._id.indexOf(zip._id) !== -1).length === 0){
      this.selectedZips.push(zip);
      this.zipService.filteredZips.next(this.selectedZips);
    }
  }

  clearSelection(){
    this.selectedZips = [];
    this.zipService.filteredZips.next([]);
  }

  changeMenu(){
    this.zipService.hideMenu.next(!this.hideMenu);
  }
}
